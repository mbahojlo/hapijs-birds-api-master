//po zmianach w pliku puscic od nowa debuger <===
let obiekt ={
    bird:{
        name:'kot',
        inne:'oko'
    },
    oko: 'dupa'
};
//zaawnsowana dekompozycja obiektu
let {bird:{name:birdname}} = obiekt;
console.log(birdname); //kot




let config =  {
            host: 'localhost',
            user: 'birdbase',
            password: 'birdbase',
            database: 'birdbase',
            charset: 'utf8',
        }

let fname;
({host:fname}=config);// przy zadeklarowanych zmiennych uzywam nawiasu()

let testtable=[10,20,30,40];
//dekompozycja tablicy
//(2) [30, 40]
let [one, two,,four]  = testtable || []
console.log(two); //20


//zaawansowana dekompozycja tablicy
let [o, t,...rest] = testtable;
console.log(rest);//(2) [30, 40]
//domylsna wartosc
let [o1, t1, , ,z=100] = testtable;
//default ze zmiana nazyw
let {host1:myhost='127.0.0.1'}=config

//destukturyzacja przy funkcji
function testDestructing({user:zezmiananazwy, password}={}){
    console.log(zezmiananazwy,password);
}

testDestructing(config);