const Hapi = require('hapi');
const routes = require('./routes');
//https://futurestud.io/tutorials/hapi-how-to-allow-multiple-authentication-strategies-for-a-route
const server = new Hapi.Server();

server.connection( {
    port: ~~process.env.PORT || 8081, 
    routes: {
        cors: true
      }
} );

// new Hapi.Server({
//     connections: {
//       routes: {
//         cors: true
//       }
//     }
//   })

server.register( require( 'hapi-auth-jwt' ), ( err ) => {

    if( !err ) {
        console.log( 'done' );
    }

    server.auth.strategy( 'token', 'jwt', {

        key: 'vZiYpmTzqXMp8PpYXKwqc9ShQ1UhyAfy',

        verifyOptions: {
            algorithms: [ 'HS256' ]
        }

    } );

    routes.forEach( ( route ) => {

        console.log( `attaching ${ route.path }` );
        server.route( route );

    } );

} );


server.start( err => {

    if( err ) {

        // Fancy error handling here
        console.error( 'Error was handled!' );
        console.error( err );

    }

    console.log( `Server started at ${ server.info.uri }` );

} );


//===
/*
const Bell = require('bell');
const Hoek = require('hoek');
const server = new Hapi.Server();
server.connection({ host: 'localhost', port: 8081 });

server.register(Bell, (err) => {

    Hoek.assert(!err, err);
    server.auth.strategy('google', 'bell', {
        provider: 'google',
        password: 'cookie_encryption_password_secure',
        isSecure: false,
        // You'll need to go to https://console.developers.google.com and set up an application to get started
        // Once you create your app, fill out "APIs & auth >> Consent screen" and make sure to set the email field
        // Next, go to "APIs & auth >> Credentials and Create new Client ID
        // Select "web application" and set "AUTHORIZED JAVASCRIPT ORIGINS" and "AUTHORIZED REDIRECT URIS"
        // This will net you the clientId and the clientSecret needed.
        // Also be sure to pass the location as well. It must be in the list of "AUTHORIZED REDIRECT URIS"
        // You must also enable the Google+ API in your profile.
        // Go to APIs & Auth, then APIs and under Social APIs click Google+ API and enable it.
        clientId: '134899263296-n8lelco57vvroh16j7gqjhjrgup67s7b.apps.googleusercontent.com',
        clientSecret: 'bCEd1K212ULztQgxdeJaQGBG',
        location: server.info.uri
    });

    server.route({
        method: '*',
        path: '/bell/door',
        config: {
            auth: {
                strategy: 'google',
                mode: 'try'
            },
            handler: function (request, reply) {

                if (!request.auth.isAuthenticated) {
                    return reply('Authentication failed due to: ' + request.auth.error.message);
                }
                reply('<pre>' + JSON.stringify(request.auth.credentials, null, 4) + '</pre>');
            }
        }
    });

    server.start((err) => {

        Hoek.assert(!err, err);
        console.log('Server started at:', server.info.uri);
    });
});
*/