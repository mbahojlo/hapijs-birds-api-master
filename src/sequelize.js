const Sequelize = require('sequelize');

const sequelize = new Sequelize('heroku_41f1bc83ce8af47', 'b5bb78a9f286c2', '63749b12', {
        host: 'us-cdbr-iron-east-05.cleardb.net',
        dialect: 'mysql',
        
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },
        
        // SQLite only
        storage: 'path/to/database.sqlite',
        
        // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
        operatorsAliases: false
    });

module.exports=sequelize;